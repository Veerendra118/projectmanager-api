﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectManager.API;
using ProjectManager.API.Controllers;
using ProjectManager.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using System;

namespace ProjectManager.API.Tests.Controllers
{
    [TestClass]
    public class ProjectsControllerTest
    {


        #region  Get Test Methods

        [TestMethod]
        public void Get()
        {
            // Arrange

            ProjectEntity t = new ProjectEntity();
            ProjectsController controller = new ProjectsController();

            // Act
            List<ProjectEntity> result = controller.GetProjects();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Count > 0);
            Assert.AreEqual(1, result.FirstOrDefault(x=>x.ProjectId==1).ProjectId);
        }

        [TestMethod]
        public void GetExistingProjectById()
        {
            // Arrange
            ProjectsController controller = new ProjectsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act
            HttpResponseMessage result = controller.GetProject(1);

            // Assert
            if (result.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                ProjectEntity t = result.Content.ReadAsAsync<ProjectEntity>().Result;
                Assert.AreEqual(1, t.ProjectId);
                Assert.IsTrue(t.TaskCount ==0);
            }
        }

        [TestMethod]
        public void GetNonExistingProjectById()
        {
            // Arrange
            ProjectsController controller = new ProjectsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act
            HttpResponseMessage result = controller.GetProject(100); // project Id which does not exists.

            // Assert
            if (!result.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, result.StatusCode);
            }
        }

        #endregion


        #region Post Test Methods

        [TestMethod]
        public void InsertProject()
        {
            // Arrange
            ProjectsController controller = new ProjectsController();

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            Random r = new Random(200);
            int id = r.Next(201, 5000);
            ProjectEntity entity = new ProjectEntity();
            entity.ProjectName = "Mission Critical Project " + id.ToString();
            entity.IsActive =true;
            entity.Priority = 30;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";
            entity.ManagerId = 1;

            // Act
            HttpResponseMessage response = controller.PostProject(entity);


            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.IsNotNull(message);
            }
        }
        [TestMethod]
        public void UniqueValidationWhileInsert()
        {
            // Arrange
            ProjectsController controller = new ProjectsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            ProjectEntity entity = new ProjectEntity();

            Random r = new Random(2000);
            int id = r.Next(2001, 5000);
            
            entity.ProjectName = "Urgent Project " + id.ToString();
            entity.IsActive = true;
            entity.Priority = 25;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";
            entity.ManagerId = 2;

            // Act
            HttpResponseMessage response = controller.PostProject(entity);

            // Assert
            if (response.IsSuccessStatusCode)
            {
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.IsNotNull(message);
            }
        }


        #endregion


        #region Put Test Methods

        [TestMethod]
        public void UpdateExistingTask()
        {
            // Arrange
            ProjectsController controller = new ProjectsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
           
            
            ProjectEntity entity = new ProjectEntity();
            entity.ProjectId = 0;
            entity.ProjectName = "Test Project";
            entity.IsActive = true;
            entity.Priority = 30;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";
            entity.ManagerId = 1;

            // Act
            var response = controller.PutProject(0, entity);

            // Assert
            if (response.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsFalse(entity.ProjectId == 1);
                Assert.AreNotEqual("First Project", entity.ProjectName);
            }


        }



        [TestMethod]
        public void UniqueValidationWhileUpdate()
        {
            // Arrange
            ProjectsController controller = new ProjectsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            ProjectEntity entity = new ProjectEntity();
            entity.ProjectId = 1;
            entity.ProjectName = "Jquery Prj";
            entity.ManagerId = 1;
            entity.Priority = 25;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";

            // Act
            HttpResponseMessage response = controller.PutProject(1, entity);

            // Assert

            if (!response.IsSuccessStatusCode)
            {
                Assert.AreNotEqual(HttpStatusCode.OK, response.StatusCode);
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.AreEqual("Project Name should be unique.", message);
            }
        }



        [TestMethod]
        public void InValidInputWhileUpdate()
        {
            // Arrange
            ProjectsController controller = new ProjectsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            ProjectEntity entity = new ProjectEntity();
            entity.ProjectId = 1988;
            entity.ProjectName = "A scrap project";
            entity.ManagerId = 1;
            entity.Priority = 25;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";
            entity.IsActive = true;

            // Act
            HttpResponseMessage response = controller.PutProject(1988, entity);


            // Assert

            if (!response.IsSuccessStatusCode)
            {
                Assert.AreNotEqual(HttpStatusCode.OK, response.StatusCode);
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.AreEqual("Invalid Project, Verify the Details.", message);
            }
        }


        [TestMethod]
        public void UpdateNonExistingTask()
        {
            // Arrange
            ProjectsController controller = new ProjectsController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            ProjectEntity entity = new ProjectEntity();
            entity.ProjectId = 1001;
            entity.ProjectName = "Mission Critical Project ";
            entity.IsActive = true;
            entity.Priority = 30;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";
            entity.ManagerId = 1;

            // Act
            var response = controller.PutProject(1001, entity);

            // Assert
            if (!response.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsFalse(entity.ProjectId == 1);
                Assert.AreNotEqual("First Project", entity.ProjectName);
            }
            //else
            //{
            //    Assert.AreNotEqual(HttpStatusCode.BadRequest, response.StatusCode);
            //}


        }

        #endregion
    }
}
