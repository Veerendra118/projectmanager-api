﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectManager.API;
using ProjectManager.API.Controllers;
using ProjectManager.Entity;
using System.Net;

namespace ProjectManager.API.Tests.Controllers
{
    [TestClass]
    public class UsersControllerTest
    {

        #region  Get Test Methods

        [TestMethod]
        public void Get()
        {
            // Arrange

            UserEntity t = new UserEntity();
            UsersController controller = new UsersController();

            // Act
            IEnumerable<UserEntity> result = controller.GetUsers();  

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Count() > 0);
            Assert.AreEqual(1, result.ElementAt(0).UserId);
        }

        [TestMethod]
        public void GetExistingUserById()
        {
            // Arrange
            UsersController controller = new UsersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act
            HttpResponseMessage result = controller.GetUser(1);

            // Assert
            if (result.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                UserEntity t = result.Content.ReadAsAsync<UserEntity>().Result;
                Assert.AreEqual(true, t.IsActive);
                Assert.IsTrue(t.EmployeeId >0);
            }
        }

        [TestMethod]
        public void GetNonExistingUserById()
        {
            // Arrange
            UsersController controller = new UsersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act
            HttpResponseMessage result = controller.GetUser(100); // UserId which does not exists.

            // Assert
            if (!result.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, result.StatusCode);
            }
        }

        #endregion


        #region Post Test Methods

        [TestMethod]
        public void InsertUser()
        {
            // Arrange
            UsersController controller = new UsersController();

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            Random r = new Random(20000);
            int id = r.Next(20001, 50000);
            UserEntity entity = new UserEntity();
            entity.FirstName = "James " + id.ToString();
            entity.EmployeeId = id;
            entity.LastName = "Bond";
            
            entity.IsActive = true;


            // Act
            HttpResponseMessage response = controller.PostUser(entity);


            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.IsNotNull(message);
            }
        }
        [TestMethod]
        public void UniqueValidationWhileInsert()
        {
            // Arrange
            UsersController controller = new UsersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            UserEntity entity = new UserEntity();

            entity.FirstName = "Hari";
            entity.LastName = "Jampala";
            entity.EmployeeId = 1001;

            // Act
            HttpResponseMessage response = controller.PostUser(entity);

            // Assert
            if (response.IsSuccessStatusCode)
            {
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.IsNotNull(message);
            }
        }


        #endregion


        #region Put Test Methods

        [TestMethod]
        public void UpdateExistingTask()
        {
            // Arrange
            UsersController controller = new UsersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            UserEntity entity = new UserEntity();
            entity.UserId = 1;
            entity.EmployeeId = 1001;
            entity.FirstName = "Ram";
            entity.LastName = "Kaneria";

            // Act
            var response = controller.PutUser(1, entity);

            // Assert
            if (response.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsFalse(entity.EmployeeId == 1);
                Assert.AreNotEqual("Shyam", entity.FirstName);
            }

        }



        [TestMethod]
        public void UniqueValidationWhileUpdate()
        {
            // Arrange
            UsersController controller = new UsersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            UserEntity t = null;

            // Act
            HttpResponseMessage result = controller.GetUser(1);

            // Assert
            if (result.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                 t = result.Content.ReadAsAsync<UserEntity>().Result;
            }

            t.EmployeeId = 1002; // EmployeeId of the another user


            // Act
            HttpResponseMessage response = controller.PutUser(2, t);


            // Assert

            if (!response.IsSuccessStatusCode)
            {
                Assert.AreNotEqual(HttpStatusCode.OK, response.StatusCode);
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.AreEqual("Invalid User, Verify the Details.", message);
            }
        }



        [TestMethod]
        public void InValidInputWhileUpdate()
        {
            // Arrange
            UsersController controller = new UsersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            UserEntity entity = new UserEntity();
            entity.UserId = 0;
            entity.FirstName = "James ";
            entity.EmployeeId = 200;
            entity.LastName = "Bond";

            entity.IsActive = true;


            // Act
            HttpResponseMessage response = controller.PutUser(0, entity);


            // Assert

            if (!response.IsSuccessStatusCode)
            {
                Assert.AreNotEqual(HttpStatusCode.OK, response.StatusCode);
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.AreEqual("Invalid User, Verify the Details.", message);
            }
        }


        [TestMethod]
        public void UpdateNonExistingUser()
        {
            // Arrange
            UsersController controller = new UsersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            UserEntity entity = new UserEntity();
            entity.UserId = 555;
            entity.FirstName = "No Where";
            entity.LastName = "Not exists";
            entity.EmployeeId =2;
            // Act
            var response = controller.PutUser(55, entity);

            // Assert
            if (!response.IsSuccessStatusCode)
            {                Assert.AreNotEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsFalse(entity.FirstName ==  "Madhav");
                Assert.AreNotEqual(1, entity.EmployeeId);
            }
            //else
            //{
            //    Assert.AreNotEqual(HttpStatusCode.BadRequest, response.StatusCode);
            //}


        }

        [TestMethod]
        public void DeleteAnExistingUser()
        {
            UsersController controller = new UsersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();


            // Act

            var response = controller.DeleteUser(37);

            
                Assert.IsNotNull(response);

            
            

        }

        #endregion


    }
}
