﻿using ProjectManager.Data;
using ProjectManager.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.BusinessLogic
{
    public class UserBAL
    {

        private static UserDAL _dalUser = new UserDAL();

        public UserEntity GetUserById(int id)
        {
            return _dalUser.GetUserById(id);

        }


        public List<UserEntity> GetUsers()
        {
            return _dalUser.GetUsers();
        }

        public StatusCode SaveUser(UserEntity usr, int id = 0)
        {
            StatusCode code;

            if (usr.UserId == 0)
            {
                code = _dalUser.InsertUser(ref usr);
            }
            else
            {
                code = _dalUser.UpdateUser(usr);
            }

            return code;
        }

        public StatusCode DeleteUser(int userId)
        {
            return _dalUser.DeleteUser(userId);
        }
    }
}
