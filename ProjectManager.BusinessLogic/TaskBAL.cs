﻿using ProjectManager.Data;
using ProjectManager.Entity;
using System;
using System.Collections.Generic;




namespace TaskManager.BusinessLayer
{
    public class TaskBAL
    {

        private static TaskDAL _dalTask = new TaskDAL();

        public TaskEntity GetTaskByTaskId(int id)
        {
            return _dalTask.GetTaskById(id);
            
        }


        public List<TaskEntity> GetTasks()
        {
            return _dalTask.GetTasks();
        }

        public StatusCode SaveTask(TaskEntity task, int id = 0)
        {
            StatusCode code;

            if (task.TaskId == 0)
            {
                code = _dalTask.InsertTask(ref task);
            }
            else
            {
                code = _dalTask.UpdateTask(task);
            }

            return code;
        }
    }
}
