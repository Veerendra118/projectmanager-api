﻿using ProjectManager.Data;
using ProjectManager.Entity;

using System.Collections.Generic;


namespace ProjectManager.BusinessLogic
{
    public class ProjectBAL
    {
        private static ProjectDAL _dalProject = new ProjectDAL();

        public ProjectEntity GetProjectsById(int id)
        {
            return _dalProject.GetProjectById(id);

        }


        public List<ProjectEntity> GetProjects()
        {
            return _dalProject.GetProjects();
        }

        public StatusCode SaveProject(ProjectEntity project, int id = 0)
        {
            StatusCode code;

            if (project.ProjectId == 0)
            {
                code = _dalProject.InsertProject(ref project);
            }
            else
            {
                code = _dalProject.UpdateProject(project);
            }

            return code;
        }

        public StatusCode DeleteProject(int projectId)
        {
            return _dalProject.DeleteProject(projectId);
        }
    }
}
