﻿using ProjectManager.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Data
{
    public class UserDAL
    {

        private Entities _dbConnection = new Entities();

        public List<UserEntity> GetUsers()
        {
            List<UserEntity> users = new List<UserEntity>();

            var dbUsers = _dbConnection.GetAllActiveUsers();

            foreach (var item in dbUsers)
            {
                users.Add(new UserEntity()
                {
                    UserId=item.UserId,
                    EmployeeId =item.EmployeeId,
                    FirstName=item.FirstName,
                    LastName=item.LastName,
                    IsActive =item.IsActive
                }
                    );
            }
            return users;
        }

        public UserEntity GetUserById(int userID)
        {
            UserEntity u = null;
            try
            {

                var item = _dbConnection.Users.FirstOrDefault(x => x.UserId == userID);
                if (item != null && item.UserId > 0)
                {
                    u = new UserEntity()
                    {
                        UserId = item.UserId,
                        FirstName = item.FirstName,
                        LastName = item.LastName,
                        EmployeeId = item.EmployeeId,
                        IsActive = item.IsActive
                    };
                }
            }
            catch (Exception)
            {
                u = null;
            }
            return u;
        }

        public StatusCode InsertUser(ref UserEntity user)
        {
            StatusCode code = StatusCode.UniqueValidation;

            try
            {
                var dbUser = new User()
                {
                    UserId = user.UserId
                       ,
                    FirstName = user.FirstName
                       ,
                    LastName = user.LastName
                       ,
                    EmployeeId = user.EmployeeId
                    
                    
                };

                if (IsUserNameUnique(user.UserId, user.UserName))
                {
                    dbUser.IsActive = true;
                    _dbConnection.Users.Add(dbUser);
                    _dbConnection.SaveChanges();
                    user.UserId= dbUser.UserId;
                    code = StatusCode.Success;
                }
                else
                {
                    code = StatusCode.UniqueValidation;
                }
            }
            catch (Exception ex)
            { code = StatusCode.Error; }

            return code;
        }

        public StatusCode UpdateUser(UserEntity usr)
        {
            StatusCode code = StatusCode.UniqueValidation;
            try
            {
                if (UserExists(usr.UserId))
                {
                    if (IsUserNameUnique(usr.UserId, usr.UserName))
                    {
                        var dbUser = _dbConnection.Users.Find(usr.UserId);
                        dbUser.FirstName = usr.FirstName;
                        dbUser.LastName = usr.LastName;
                        dbUser.EmployeeId = usr.EmployeeId;
                        dbUser.IsActive = true;

                        _dbConnection.Entry(dbUser).State = EntityState.Modified;
                        _dbConnection.SaveChanges();
                        code = StatusCode.Success;
                    }
                }
            }
            catch (Exception ex)
            {
                code = StatusCode.Error;
            }
            return code;
        }


        //ToDO : Delete Method

        public StatusCode DeleteUser(int userId)
        {

            StatusCode code = StatusCode.Success;
            try
            {
                int i = _dbConnection.DeleteUser(userId);

            }
            catch (Exception ex)
            {
                code = StatusCode.Error;
            }
            return code;
        }

        private bool UserExists(int id)
        {
            return _dbConnection.Users.Count(e => e.UserId == id && e.UserId> 0) > 0;
        }

        private bool IsUserNameUnique(int id, string name)
        {
            bool isUnique = true;
            int noOfMatches = 0;
            noOfMatches = (from t in _dbConnection.Users where t.UserId!= id && t.FirstName + " " +t.LastName == name select t).Count();
            isUnique = (noOfMatches == 0);

            return isUnique;
        }
    }
}
