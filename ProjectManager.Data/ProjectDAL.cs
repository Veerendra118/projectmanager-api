﻿using ProjectManager.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ProjectManager.Data
{
    public class ProjectDAL
    {

        private Entities _dbConnection = new Entities();


        public List<ProjectEntity> GetProjects()
        {
            List<ProjectEntity> projects = new List<ProjectEntity>();

            var dbProjects = _dbConnection.GetActiveProjects();

            foreach (var item in dbProjects)
            {
                projects.Add(new ProjectEntity()
                {
                     ProjectId = item.ProjectId
                    ,ProjectName =item.ProjectName
                    ,StartDate =item.StartDate
                    ,CompletedCount =item.CompletedCount
                    ,Priority = item.Priority
                    ,TaskCount =item.totalCount
                    ,EndDate = item.EndDate
                    ,Status = item.Status
                    ,IsActive=item.IsActive
                });
            }
            return projects;
        }




        public ProjectEntity GetProjectById(int projectId)
        {
            ProjectEntity t = null;
            try
            {
                var item = _dbConnection.GetProjectByProjectId(projectId).First();

                if (item != null && item.ProjectId > 0)
                {
                    t = new ProjectEntity()
                    {
                        ProjectId = item.ProjectId
                       ,
                        ProjectName = item.ProjectName
                       ,
                        StartDate = item.StartDate
                       ,
                        Priority = item.Priority
                       ,
                        EndDate = item.EndDate
                       ,
                        Status = item.Status
                        ,ManagerId =item.ProjectManagerID
                        ,ManagerName =item.ManagerName
                    };
                }
            }
            catch (Exception)
            {
                t = null;
            }
            return t;
        }

        public StatusCode InsertProject(ref ProjectEntity prj)
        {
            StatusCode code = StatusCode.UniqueValidation;

            try
            {
                var dbProject = new Project()
                {
                    ProjectId = prj.ProjectId
                       ,
                    ProjectName = prj.ProjectName
                       ,
                    StartDate = prj.StartDate
                       ,
                    Priority = prj.Priority
                       ,
                    EndDate = prj.EndDate
                       ,
                    Status = prj.Status
                    ,ProjectManagerID =prj.ManagerId,
                    IsActive=prj.IsActive
                };

                if (IsProjectNameUnique(prj.ProjectId, prj.ProjectName))
                {
                    _dbConnection.Projects.Add(dbProject);
                    _dbConnection.SaveChanges();
                    prj.ProjectId = dbProject.ProjectId;
                    code = StatusCode.Success;
                }
                else
                {
                    code = StatusCode.UniqueValidation;
                }
            }
            catch (Exception ex)
            { code = StatusCode.Error; }

            return code;
        }

        public StatusCode UpdateProject(ProjectEntity prj)
        {
            StatusCode code = StatusCode.UniqueValidation;
            try
            {
                if (ProjectExists(prj.ProjectId))
                {
                    if (IsProjectNameUnique(prj.ProjectId, prj.ProjectName))
                    {
                        var dbProject = _dbConnection.Projects.Find(prj.ProjectId);
                        dbProject.ProjectName = prj.ProjectName;
                        dbProject.Priority = prj.Priority;
                        dbProject.StartDate = prj.StartDate;
                        dbProject.EndDate = prj.EndDate;
                        dbProject.Status = prj.Status;
                        dbProject.ProjectManagerID = prj.ManagerId;
                        dbProject.IsActive = prj.IsActive;

                        _dbConnection.Entry(dbProject).State = EntityState.Modified;
                        _dbConnection.SaveChanges();
                        code = StatusCode.Success;
                    }
                }
            }

            catch (Exception ex)
            {
                code = StatusCode.Error;
            }
            return code;
        }

        //ToDO : Delete Method

        public StatusCode DeleteProject(int projectId)
        {
            StatusCode code = StatusCode.Success;
            try
            {
                int i = _dbConnection.DeleteProject(projectId);

            }
            catch (Exception ex)
            {
                code = StatusCode.Error;
            }
            return code;
        }

        private bool ProjectExists(int id)
        {
            return _dbConnection.Projects.Count(e => e.ProjectId == id && e.ProjectId > 0) > 0;
        }

        private bool IsProjectNameUnique(int id, string prjName)
        {
            bool isUnique = true;
            int noOfMatches = 0;
            noOfMatches = (from t in _dbConnection.Projects where t.ProjectId != id && t.ProjectName == prjName select t).Count();
            isUnique = (noOfMatches == 0);

            return isUnique;
        }


    }
}
