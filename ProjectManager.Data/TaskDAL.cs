﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using ProjectManager.Entity;

namespace ProjectManager.Data
{

        public class TaskDAL
        {


            private Entities _dbConnection = new Entities();


            public List<TaskEntity> GetTasks()
            {
                List<TaskEntity> tasks = new List<TaskEntity>();

                var dbtasks = _dbConnection.GetAllTasks();

                foreach (var item in dbtasks)
                {
                    tasks.Add(new TaskEntity()
                    {
                        TaskId = item.TaskId
                                                  ,
                        ParentTaskId = item.ParentTaskId
                                                  ,
                        ParentTaskName = item.ParentTaskName
                                                ,
                        TaskName = item.TaskName
                                                    ,
                        Priority = item.Priority
                                                   ,
                        StartDate = item.StartDate,
                        EndDate = item.EndDate,
                        Status = item.Status,
                        ProjectId=item.ProjectId,
                        ProjectName=item.ProjectName,
                        AssignedId=item.UserId,
                       taskUserName =item.FirstName + " " +item.LastName
                    });
                }
                return tasks;
            }




            public TaskEntity GetTaskById(int taskId)
            {
                TaskEntity result = null;
                try
                {
                    var t = _dbConnection.GetTaskDetailsByTaskId(taskId).First();

                    if (t != null && t.TaskId > 0)
                    {
                        result = new TaskEntity()
                        {
                            TaskId = t.TaskId,
                            TaskName = t.TaskName,
                            ParentTaskId = t.ParentTaskId,
                            ParentTaskName = t.ParentTaskName,
                            Priority = t.Priority,
                            StartDate = t.StartDate,
                            EndDate = t.EndDate,
                            Status = t.Status,
                            ProjectId = t.ProjectId,
                            ProjectName = t.ProjectName,
                            AssignedId = t.UserId,
                            taskUserName = t.AssignedUser
                        };
                    }
                }
                catch (Exception)
                {
                    result = null;
                }
                return result;
            }

            public StatusCode InsertTask(ref TaskEntity task)
            {
                StatusCode code = StatusCode.UniqueValidation;

                try
                {
                    var dbTask = new Task()
                    {
                        TaskId = task.TaskId
                                                ,
                        TaskName = task.TaskName
                                                ,
                        ParentTaskId = task.ParentTaskId
                                                ,
                        Priority = task.Priority
                                                ,
                        StartDate = task.StartDate
                                                ,
                        EndDate = task.EndDate
                                                ,
                        Status = task.Status,
                         ProjectId = task.ProjectId,
                        
                        AssignedTo = task.AssignedId ,
                        IsActive =true
                        
                        
                    };

                    if (IsTaskNameUnique(task.TaskId, task.TaskName))
                    {

                        _dbConnection.Tasks.Add(dbTask);
                        _dbConnection.SaveChanges();
                        task.TaskId = dbTask.TaskId;
                        code = StatusCode.Success;
                    }
                    else
                    {
                        code = StatusCode.UniqueValidation;
                    }
                }
                catch (Exception ex)
                { code = StatusCode.Error; }

                return code;
            }

            public StatusCode UpdateTask(TaskEntity task)
            {
                StatusCode code = StatusCode.UniqueValidation;
                try
                {
                    if (TaskExists(task.TaskId))
                    {
                        if (IsTaskNameUnique(task.TaskId, task.TaskName))
                        {
                            var dbTask = _dbConnection.Tasks.Find(task.TaskId);
                            dbTask.TaskName = task.TaskName;
                            dbTask.ParentTaskId = task.ParentTaskId;
                            dbTask.Priority = task.Priority;
                            dbTask.StartDate = task.StartDate;
                            dbTask.EndDate = task.EndDate;
                            dbTask.Status = task.Status;
                            dbTask.ProjectId = task.ProjectId;
                            dbTask.AssignedTo = task.AssignedId;
                            
                            _dbConnection.Entry(dbTask).State = EntityState.Modified;
                            _dbConnection.SaveChanges();
                            code = StatusCode.Success;
                        }
                        
                    }
                }
               
                catch (Exception ex)
                {
                    code = StatusCode.Error;
                }
                return code;
            }

            private bool TaskExists(int id)
            {
                return _dbConnection.Tasks.Count(e => e.TaskId == id && e.TaskId > 0) > 0;
            }

            private bool IsTaskNameUnique(int id, string taskName)
            {
                bool isUnique = true;
                int noOfMatches = 0;
                noOfMatches = (from t in _dbConnection.Tasks where t.TaskId != id && t.TaskName == taskName select t).Count();
                isUnique = (noOfMatches == 0);

                return isUnique;
            }


        }
    

}
