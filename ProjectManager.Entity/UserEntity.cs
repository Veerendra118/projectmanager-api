﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Entity
{
    [Serializable]
   public class UserEntity
    {

        public int UserId { get; set; }
        public int EmployeeId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public bool IsActive { get; set; }

        public string UserName {

          get {
                return FirstName + " " + LastName;
            }
        }
    }
}
