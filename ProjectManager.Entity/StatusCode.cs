﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Entity
{
   
    public enum StatusCode
    {
        Success = 0,
        Error = 1,
        UniqueValidation = 2,
        ConcurrancyError = 3
    }
}
