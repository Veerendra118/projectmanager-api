﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Entity
{
    [Serializable]
    public class ProjectEntity
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public bool IsActive { get; set; } // This is to identify whether project is suspended or not

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public byte Priority { get; set; }


        public int TaskCount { get; set; }

        public int CompletedCount { get; set; }

        public int ManagerId { get; set; }

        public string ManagerName { get; set; }

        public string Status { get; set; } // this is to know the status of the project OPEN or CLOSED
        public bool IsProjectCompleted
        {
            get
            {
                return (Status == "OPEN" ? false : true);
            }
        }


    }
}
