﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Entity
{
    [Serializable]
    public class TaskEntity
    {
        public int TaskId { get; set; }
        public int ParentTaskId { get; set; }
        public string TaskName { get; set; }
        public string ParentTaskName { get; set; }
        public byte Priority { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Status { get; set; }
        public bool IsTaskEnded
        {
            get
            {
                return (Status == "OPEN" ? false : true);
            }
        }

        public int? AssignedId { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }

        public string taskUserName { get; set; }

    }
 }
