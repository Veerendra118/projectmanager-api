﻿using System.Collections.Generic;
using NBench;
using System.Net.Http;
using System;
using ProjectManager.Entity;

namespace ProjectManager.PerformanceTests
{
    public class DictionaryThroughputTests
    {

        private const string appUrl = "http://localhost/ProjectManagerService/api/";

        private static HttpClient client = new HttpClient() { BaseAddress = new Uri(DictionaryThroughputTests.appUrl) };
        private readonly Dictionary<int, TaskEntity[]> dictionary = new Dictionary<int, TaskEntity[]>();

        private const string GetTasksCounter = "GetTasks";
        private Counter GetCounter;
        private int key;

        private const int AcceptableMinAddThroughput = 200;

        [PerfSetup]
        public void Setup(BenchmarkContext context)
        {
            GetCounter = context.GetCounter(GetTasksCounter);
            key = 0;
        }

        [PerfBenchmark(RunMode = RunMode.Throughput, TestMode = TestMode.Test)]
        [CounterThroughputAssertion(GetTasksCounter, MustBe.GreaterThan, AcceptableMinAddThroughput)]
        public void AddThroughput_ThroughputMode(BenchmarkContext context)
        {

            var responseTask = client.GetAsync("tasks");
            responseTask.Wait();

            var result = responseTask.Result;
            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<TaskEntity[]>();
                readTask.Wait();
                dictionary.Add(key++, readTask.Result);
            }

            
            GetCounter.Increment();
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [CounterThroughputAssertion(GetTasksCounter, MustBe.GreaterThan, AcceptableMinAddThroughput)]
        public void AddThroughput_IterationsMode(BenchmarkContext context)
        {
            for (var i = 0; i < 2000; i++)
            {
                var responseTask = client.GetAsync("tasks");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<TaskEntity[]>();
                    readTask.Wait();
                    dictionary.Add(key++, readTask.Result);
                    GetCounter.Increment();
                }
            }
        }

        [PerfCleanup]
        public void Cleanup(BenchmarkContext context)
        {
            dictionary.Clear();
        }
    }
}
