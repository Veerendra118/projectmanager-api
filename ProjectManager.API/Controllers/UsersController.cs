﻿using ProjectManager.BusinessLogic;
using ProjectManager.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ProjectManager.API.Controllers
{
    public class UsersController : ApiController
    {
        private UserBAL _userBal = new UserBAL();

        public List<UserEntity> GetUsers()
        {
            return _userBal.GetUsers();
        }


        //GET: api/Users/5
        [ResponseType(typeof(UserEntity))]
        public HttpResponseMessage GetUser(int id)
        {
            UserEntity user = _userBal.GetUserById(id);
            if (user == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "User Details Not found");

            return Request.CreateResponse<UserEntity>(HttpStatusCode.OK, user);
        }


        // PUT: api/Users/5
        [ResponseType(typeof(string))]
        public HttpResponseMessage PutUser(int id, [FromBody] UserEntity user)
        {
            if (id != user.UserId)
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "Invalid User, Verify the Details.");

            StatusCode status = _userBal.SaveUser(user);

            if (status == Entity.StatusCode.Success)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "User Details are saved successfully");
            }
            else if (status == Entity.StatusCode.ConcurrancyError)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Concurrency Issue, Please try again.");
            }
            else if (status == Entity.StatusCode.UniqueValidation)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "User Name should be unique.");
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "Error Occured.");
            }
        }

        // POST: api/Users
        [ResponseType(typeof(string))]
        public HttpResponseMessage PostUser([FromBody] UserEntity user)
        {


            StatusCode status = _userBal.SaveUser(user);

            if (status == Entity.StatusCode.Success)
            {
                //return Request.CreateResponse<UserEntity>(HttpStatusCode.OK, user);
                return Request.CreateResponse<string>(HttpStatusCode.OK, "User Details are saved successfully.");
            }
            else if (status == Entity.StatusCode.ConcurrancyError)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Concurrency Issue, Please try again.");
            }
            else if (status == Entity.StatusCode.UniqueValidation)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "User Name should be unique.");
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "Error Occured.");
            }
        }
        
        [HttpDelete]
        [ResponseType(typeof(UserEntity))]
        public IHttpActionResult DeleteUser(int id)
        {
            StatusCode status = _userBal.DeleteUser(id);
            UserEntity usr = new UserEntity(); 

            if (status == Entity.StatusCode.Success)
            {
                //return Request.CreateResponse<UserEntity>(HttpStatusCode.OK, user);
                return Ok(usr);
            }
            else
            {
                return NotFound();
            }

        }

    }
}
