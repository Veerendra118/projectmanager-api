﻿using ProjectManager.BusinessLogic;
using ProjectManager.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;


namespace ProjectManager.API.Controllers
{
    //[EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    
    public class ProjectsController : ApiController
    {

        private ProjectBAL _projectBal = new ProjectBAL();

        
        public List<ProjectEntity> GetProjects()
        {
            return _projectBal.GetProjects();
        }


        //GET: api/Projects/5
        [ResponseType(typeof(ProjectEntity))]
        public HttpResponseMessage GetProject(int id)
        {
            ProjectEntity project = _projectBal.GetProjectsById(id);
            if (project == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "Project Details Not found");

            return Request.CreateResponse<ProjectEntity>(HttpStatusCode.OK, project);
        }


        // PUT: api/Projects/5
        [ResponseType(typeof(string))]
        public HttpResponseMessage PutProject(int id, [FromBody] ProjectEntity project)
        {
            if (id != project.ProjectId)
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "Invalid Project, Verify the Details.");

            StatusCode status = _projectBal.SaveProject(project);

            if (status == Entity.StatusCode.Success)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Project Details are saved successfully");
            }
            else if (status == Entity.StatusCode.ConcurrancyError)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Concurrency Issue, Please try again.");
            }
            else if (status == Entity.StatusCode.UniqueValidation)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Project Name should be unique.");
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "Error Occured.");
            }
        }

        // POST: api/Projects
        [ResponseType(typeof(string))]
        public HttpResponseMessage PostProject([FromBody] ProjectEntity project)
        {


            StatusCode status = _projectBal.SaveProject(project);

            if (status == Entity.StatusCode.Success)
            {
                //return Request.CreateResponse<ProjectEntity>(HttpStatusCode.OK, project);
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Project Details are saved successfully.");
            }
            else if (status == Entity.StatusCode.ConcurrancyError)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Concurrency Issue, Please try again.");
            }
            else if (status == Entity.StatusCode.UniqueValidation)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Project Name should be unique.");
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "Error Occured.");
            }
        }


        [HttpDelete]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteProject(int id)
        {
            StatusCode status = _projectBal.DeleteProject(id);
            ProjectEntity prj= new ProjectEntity();

            if (status == Entity.StatusCode.Success)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Project Details are deleted successfully.");
                //return Ok(prj);
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "Error Occured.");
            }

        }

    }
}
